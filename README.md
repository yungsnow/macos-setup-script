# MacOS Setup Script

This script:
- spoof the MAC-Adress every boot and disable Wi-Fi interface at logout ([Source](https://sunknudsen.com/privacy-guides/how-to-spoof-mac-address-and-hostname-automatically-at-boot-on-macos))
- installs apps i use
    - Console:
        - brew
        - neofetch
        - htop
        - figlet
    - Graphical:
        - Firefox
        - Tor-Browser
        - iTerm2
        - Rectangle
        - TextMate
        - Alfred
        - AppCleaner
        - Spotify
        - Visual Studio Code
        - ExifCleaner
